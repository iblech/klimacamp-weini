---
layout: page
title:  "26.05.2021: Weingartenwald-Besetzer*innen entschärfen Situation"
date:   2021-05-26 03:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 1
permalink: /pressemitteilungen/2021-05-26-Entschaerfung/
---

*Pressemitteilung aus der Weingartenwaldbesetzungsgemeinschaft am 26. Mai 2021*

# Weingartenwald-Besetzer\*innen entschärfen Situation

Unmittelbar nach den ersten Presseberichten am gestrigen Dienstag suchte die
Polizei die junge Besetzungsgemeinschaft im Weingartenwald auf. Laut
Polizeiangaben gegenüber den Klimagerechtigkeitsaktivist\*innen würde die
Besetzung durch ein angefordertes Sondereinsatzkommando noch am heutigen
Mittwoch beendet werden, und unabhängig von ihrer Kooperationsbereitschaft
und trotz Personalienangabe würden alle Aktivist\*innen vorübergehend in
Gewahrsam genommen werden.

Die Aktivist\*innen entschlossen sich daraufhin zur Entschärfung der Situation,
in dem sie ankündigten, am frühen Morgen das gerade erst entstandene
Baumhausdorf wieder abzubauen und die Bäume zu verlassen. "Über die nächsten
Wochen wäre aus unserem Baumhausdorf ein vielfältiger Ort der Begegnung
geworden", so Waldbesetzer Heiko Zimmermann (24). "Es ist schade, dass es nun
nicht dazu kommt, möchten aber dennoch die Situation entschärfen. Ein noch
früherer nächtlicher Abbau wäre wegen Dunkelheit und Müdigkeit zu gefährlich",
so Waldbesetzer Heiko Zimmermann (24).

"Dass gleich das Sondereinsatzkommando bemüht wird, nur um eine Handvoll
Jugendlicher aus einem Baumhaus zu räumen, und wir trotz Abbau in Gewahrsam
genommen werden, zeigt eindrucksvoll, wie sich unser klimazerstörerische System
ein letztes Mal aufbäumt", so Zimmermanns Mitstreiterin Charlotte Ebert (17).
"Wir sind viele, und wir haben das Bundesverfassungsgericht klar auf unserer
Seite. Der heutige Abbau ist nicht der Ende des Widerstands im Weingartenwald,
sondern sein Anfang", so Ebert weiter.

"Solange unsere Regierungen Kurzzeitinteressen über unsere Lebensgrundlagen
stellen, ist Engagement in Form von Waldbesetzungen leider nötig", erklärt
Zimmermann. "Die Gerichte können die Verfahren nicht schnell genug bewältigen."
Damit spielt er auf die erfolgreiche Besetzung des Hambacher Forsts an, bei dem
das zuständige Oberverwaltungsgericht die Rechtswidrigkeit der Rodung erst nach
sechs Jahren Besetzung feststellen konnte. "Hätte unsere aktivistische
Vorgängergeneration damals den Hambi nicht besetzt, gäbe es ihn heute nicht
mehr", so Zimmermann.


## Richtigstellung zur Polizeiinteraktion

Stellenweise wurde behauptet [1], die Aktivist\*innen sprächen nicht mit der
Polizei. Diese Aussage ist falsch. "Unser Verhältnis zu den am Boden wartenden
Polizeibeamten ist gut", so Samuel Bosch (18) vom Ravensburger Klimacamp, der
die Aktion mit seiner Erfahrung unterstützt. "Für die Nacht leihten wir den
Polizeikräften eine unserer Hängematten."

[1] https://www.schwaebische.de/landkreis/bodenseekreis/meersburg_artikel,-klimaaktivisten-wollen-bau-der-b-31-neu-verhindern-_arid,11367551.html


## Pressekonferenz

Gestern kündigten wir für den heutigen Mittwoch (26.5.2021) um 11:00 Uhr eine
Pressekonferenz bei der Besetzung an. Diese Pressekonferenz soll weiterhin
stattfinden, nun am Boden und aufgrund der in Aussicht gestellten
Ingewahrsamnahme der Baumbewohner\*innen in anderer personeller Besetzung.

Eine Garantie kann wegen des laufenden Polizeieinsatzes nicht gegeben werden,
doch über Ingo Blechschmidt (+49 176 95110311) oder den öffentlichen
Telegram-Kanal https://t.me/weini_bleibt kann jederzeit der aktuelle Stand in
Erfahrung gebracht werden.

Nach den Erfahrungen im Dannenröder Wald ist mit Polizeibewegung ab 7:00 Uhr in
der Früh zu rechnen.


## Koordinaten der Besetzung und Anreise

https://weini.klimacamp.eu/anreise/


## Kontakt

Ingo Blechschmidt (+49 176 95110311) stellt den Kontakt zum Baumhausdorf her.
Es gibt keine autorisierte Gruppe und kein beschlussfähiges Gremium, das
"offizielle Gruppenmeinungen" für die Besetzung beschließen könnte. Die
Menschen in der Besetzung und ihrem Umfeld haben vielfältige und teils
kontroverse Meinungen. Diese Meinungsvielfalt soll nicht zensiert werden,
sondern gleichberechtigt nebeneinander stehen. Kein Text und keine Aktion
spricht für die ganze Besetzung oder wird notwendigerweise von der ganzen
Besetzung gut geheißen.
