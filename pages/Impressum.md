---
layout: home
title: Impressum
permalink: /impressum/
nav_order: 100
---

# Impressum

Diese Webseite wird gepflegt von: Ingo Blechschmidt, [+49 176 95110311](tel:+4917695110311),
[iblech@web.de](mailto:iblech@web.de). Diese Person stellt auch gerne den
Kontakt zu den Baumhäusern her. Die Waldbesetzungsgemeinschaft wird von
Aktivist\*innen diverser Klimagerechtigkeitsbewegungen gemeinschaftlich
organisiert. In Deutschland gibt es [noch viele weitere
Klimacamps](https://klimacamp.eu/) und Solibaumhäuser für den
[Danni](https://waldstattasphalt.blackblogs.org/).
