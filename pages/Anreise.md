---
layout: home
title: Anreise
permalink: /anreise/
nav_order: 20
---

# Anreise

🧭 [Koordinaten:](https://www.google.com/maps?q=47.689924,9.325135) 47.689924, 9.325135 bzw. 47°41'23.7"N 9°19'30.5"O


## Ab Bahnhof Friedrichshafen

🚍 Bushaltestelle [Hagnau am Bodensee West](https://www.fahrplan.guru/haltestelle/deutschland/baden-wuerttemberg/hagnau-am-bodensee/hagnau-am-bodensee-west) (Linie 7395), ab dort 2,3 km zu Fuß (ca. 20 Minuten)


## Ab Bahnhof Ravensburg oder Konstanz

🚍 Bushaltestelle [Markdorf-Ittendorf](https://www.fahrplan.guru/haltestelle/deutschland/baden-wuerttemberg/markdorf/markdorf-ittendorf) (Linie 700), ab dort 2,1 km zu Fuß (ca. 20 Minuten)


## Mitfahrbörse

💬 [Mitfahrbörse auf Telegram](https://t.me/joinchat/LPQQEKhhnNZiZjFi)
