---
layout: home
title: Start
permalink: /
nav_order: 10
---

<img src="/trailer.jpeg" width="640" height="360" style="width: 100%">

# Mit unserer Besetzung verhindern wir die Rodung des Weingartenwalds

Die Bundesregierung plant die teilweise Rodung des Weingartenwalds bei Salem am
Bodensee um Platz zu machen für den Neubau der Bundesstraße B31.

In Zeiten der Klimakrise können wir nicht intakte Wälder mit ihren wertvollen
Böden roden, schon gar nicht für einen Straßenneubau, der durch eine soziale
Mobilitätswende sowieso hinfällig wird. Schiene ausbauen, Bundesstraße
rückbauen!

Deswegen besetzen wir seit dem 24.5.2021 den Wald. Wir appellieren nicht länger an
die Politik, sondern stellen uns der Klima- und Umweltzerstörung mit unseren
Baumhäusern in den Weg.

* Wir sind Schüler\*innen, Student\*innen, Azubis und Berufstätige aus der
  Region. Dank Online-Vorlesungen und Home Office können wir auch aus dem Wald
  unserer Arbeit nachgehen. Einige von uns verschreiben sich derzeit auch
  nahezu vollständig dem Klimagerechtigkeitsaktivismus und erachten ihre Zeit
  im Weini als freiwilliges ökologisches Jahr.

* Seit Jahren engagieren sich diverse Bürger\*inneninitiativen und
  Umweltverbände für den Erhalt des Weingartenwalds. Wir bauen auf ihrer
  kontinuierlichen Arbeit und ihrem wertvollen Einsatz auf und sind ihnen dafür
  sehr dankbar.

* Hintergrundinformationen zum umstrittenen Neubau der B31 stellt die
  [Arbeitsgemeinschaft "B31 -- Ausbau vor Neubau!"](https://www.b31-ausbau-vor-neubau.de/) zusammen.

* Wie wir sicher klettern und baumschonend Baumhäuser errichten können, lernten
  viele von uns im besetzten [Altdorfer Wald](https://ravensburg.klimacamp.eu/altdorfer-wald/).

ℹ️ [Infokanal auf Telegram](https://t.me/weini_bleibt)<br>
💬 [Austauschgruppe auf Telegram](https://t.me/joinchat/Ejj1L2FSsYM1MmM6)

**Hinweis:** *Diese Website wurde nicht mit der gesamten Besetzung
abgesprochen. Es gibt keine autorisierte Gruppe und kein beschlussfähiges
Gremium, das "offizielle Gruppenmeinungen" für die Besetzung beschließen
könnte. Die Menschen in der Besetzung und ihrem Umfeld haben vielfältige und
teils kontroverse Meinungen. Diese Meinungsvielfalt soll nicht zensiert werden,
sondern gleichberechtigt nebeneinander stehen. Kein Text und keine Aktion
spricht für die ganze Besetzung oder wird notwendigerweise von der ganzen
Besetzung gut geheißen.*

<script data-no-instant>
  {% include instantclick.min.js %}
  InstantClick.init();
</script>
