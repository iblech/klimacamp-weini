---
layout: home
title: "Für Aktivist*innen"
permalink: /leben/
nav_order: 40
---

# Im Wald leben

Du bist willkommen! Trotz Bedrohung durch die Klimakrise versuchen wir, eine
gute Stimmung zu bewahren. 😊


## Im Wald haben wir

* 🏕 bezugsfertige Baumhäuser
* 🧶 genug Poly zum Bauen :-)
* 🧑‍🎓 gegenseitige Wissens- und Fertigkeitsweitergabe (insbesondere Baumhausbau)
* 👩‍💻 Strom (aus Batterie, ab 1.6.) und WLAN (aus LTE-Router) für Schule und Arbeiten
* 🥙 leckeres warmes veganes Essen
* 👩‍🦳 Unterstützung durch Bürgis
* 🎨 eine dynamische unterstützende Kreativaktiviszene
* 🕊 bisher und auf absehbare Zeit wohl keine Polizeipräsenz (Rodung erst ab Oktober)
* 🔐 Möglichkeit, bei Bürgis Dinge zu verwahren (etwa Ausweisdokumente)
* 🌳 einen wunderbaren Lebensraum, der dank uns bleiben wird

Ob du nur für ein paar Stunden vorbeischauen willst oder unbefristet bleiben
möchtest -- wir freuen uns auf dich!


## Mitbringen

Du brauchst daher eigentlich nur Schlafsack und Isomatte mitzubringen. (Können
wir dir auch ausleihen, nimm dazu aber [vorab Kontakt mit uns
auf](https://t.me/joinchat/Ejj1L2FSsYM1MmM6), damit wir ganz
sicher was für dich da haben.)


## Rechtshilfe

* Der Aufenthalt in Baumhäusern ist eine Ordnungswidrigkeit, sofern nicht
  Versammlungsrecht greift.
* Zudem könnte ein Verstoß gegen die Corona-Verordnung konstruiert werden. Das
  wäre ebenfalls eine Ordnungswidrigkeit.
* Ebenfalls ist es keine Straftat, wenn du auch nach Aufforderung den Wald
  nicht verlässt (Platzverweis).

Sollten im Rahmen der Waldbesetzung rechtliche Schritte gegen dich eingeleitet
werden oder solltest du aus anderen Gründen rechtliche Hilfe benötigen, stehen
unsere solidarischen Anwält\*innen für dich zur Verfügung. Außerdem werden
etwaige Bußgelder solidarisch getragen.

**Ermittlungsausschuss: +49 176 95110311**

**[Rechtshilfebroschüren](https://ravensburg.klimacamp.eu/rechtshilfe/)**
