---
layout: home
title: Spenden
permalink: /spenden/
nav_order: 80
---

# Spenden

Wir sind gerade dabei, ein Spendenkonto einzurichten.

In der Zwischenzeit gerne direkt Kontakt zu Ingo Blechschmidt aufnehmen,
[iblech@web.de](mailto:iblech@web.de), +49 176 95110311.
