---
layout: home
title: Unterstützen
permalink: /unterstuetzen/
nav_order: 30
---

# Die Waldbesetzung unterstützen

Wir freuen uns, dass du die Waldbesetzung unterstützen möchtest! Wir wissen
jeden Beitrag wahnsinnig zu schätzen und sind dir sehr
dankbar!

So kannst du konkret helfen:

* 🗣 Im Freundes- und Bekanntenkreis über Klimagerechtigkeit, die dringend nötige
  Mobilitätswende sowie den Altdorfer Wald sprechen.
* ✉️ Leser\*innenbriefe schreiben.
* 🥙 Essen vorbeibringen (fast alle von uns versuchen, vegan zu leben).
* 🧶 Paletten, Holzdielen und Bretter sowie lange Nägel und Werkzeug
  mitbringen.
* 🔋 Unsere Batterien, die wir für unsere Laptops benötigen (Schule bzw. Home
  Office), aufladen: beim Wald abholen, später wieder zurückbringen. Ladegerät
  haben wir.
* 💶 [Geld spenden](/spenden/) (insbesondere für Baumhausmaterialien und
  etwaige Rechtskosten)
