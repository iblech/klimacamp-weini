---
layout: home
title: Pressespiegel
permalink: /pressespiegel/
nav_order: 60
---

# Pressespiegel

## 26.5.2021
* Südkurier: [Klimaaktivisten geben Camp im Weingartenwald auf. Doch der Protest gegen die Planung der B 31-neu soll weitergehen](https://www.suedkurier.de/region/bodenseekreis/bodenseekreis/klimaaktivisten-geben-camp-im-weingartenwald-auf;art410936,10817211) ([Foto](https://ravensburg.klimacamp.eu/pressespiegel/suedkurier-410936,10817211.jpeg))
* Schwäbische: [Protest wegen B31-neu schnell vorbei - Baumbesetzer räumen ihr Lager](https://www.schwaebische.de/landkreis/bodenseekreis/meersburg_artikel,-baumbesetzer-raeumen-ihr-lager-im-weingartenwald-_arid,11367926.html) ([Foto](https://ravensburg.klimacamp.eu/pressespiegel/schwaebische-11367926.jpeg))
* Schwäbische: [Besetzung wegen B31-neu: Klimaaktivisten bauen ihr Lager wieder ab](https://www.schwaebische.de/landkreis/bodenseekreis/meersburg_artikel,-klimaaktivisten-bauen-ihr-lager-wieder-ab-_arid,11367615.html) ([Foto](https://ravensburg.klimacamp.eu/pressespiegel/schwaebische-11367615.jpeg))

## 25.5.2021
* Ravensburger Spectrum: [Klimastreit: Weingartenwald bei Salem am See besetzt - "Bevor die Bundesregierung unseren Wald roden kann, muss sie uns erst räumen."](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3079571/klimastreit-weingartenwald-bei-salem-am-see-besetzt---bevor-die-bundesregie)
* Südkurier: [Klimaaktivisten besetzen seit Montag den Weingartenwald im Bodenseekreis](https://www.suedkurier.de/region/bodenseekreis/meersburg/klimaaktivisten-besetzen-seit-montag-den-weingartenwald-im-bodenseekreis;art372493,10816791) ([Foto](https://ravensburg.klimacamp.eu/pressespiegel/suedkurier-372493,10816791.jpeg))
* Südkurier: [Geht es hier noch ums Klima? In Ravensburg wird Kritik an der von Baumbesetzern gewählten Form des Protests laut](https://www.suedkurier.de/region/bodenseekreis/bodenseekreis/geht-es-hier-noch-ums-klima-in-ravensburg-wird-kritik-an-der-von-baumbesetzern-gewaehlten-form-des-protests-laut;art410936,10815331) ([Foto](https://ravensburg.klimacamp.eu/pressespiegel/suedkurier-410936,10815331.jpeg))
* Schwäbische: [Nach Altdorfer Wald und Ravensburg - Nun besetzen Klimaaktivisten Bäume an der B31-neu](https://www.schwaebische.de/landkreis/bodenseekreis/meersburg_artikel,-klimaaktivisten-wollen-bau-der-b-31-neu-verhindern-_arid,11367551.html) ([Foto](https://ravensburg.klimacamp.eu/pressespiegel/schwaebische-11367551.jpeg))
* Schwäbische: [Klimaaktivisten besetzen Weingartenwald am Bodensee](https://www.schwaebische.de/sueden/baden-wuerttemberg_video,-klimaaktivisten-besetzen-weingartenwald-am-bodensee-_vidid,159580.html)
